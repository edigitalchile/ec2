# REQUIRED PARAMETERS

variable azs {
  type        = list(string)
  description = "Lista de zonas de disponibilidad donde que se quieren crear recursos"

}

variable ami {
  type        = string
  description = "Imagen que se usara en las instancias"
}

variable subnets_ids {
  type        = list(string)
  description = "Ids de las subnets donde de crearan las instancias"
}

variable key_name {
  type        = string
  description = "Nombre de key publica que se asociara a las instancias"
}


# OPTIONAL PARAMETERS

variable prefix_instance {
  type        = string
  default     = "instance-"
  description = "Nombre que tendra cada instancia. A este nombre se le agregara el numero de la instancia creada"
}

variable instance_type {
  type        = string
  default     = "t2.micro"
  description = "Tipo de instancia que se crearan"
}


variable instance_quantity {
  type        = number
  default     = 1
  description = "Cantidad de instancias a crear"
}

variable vpc_security_group_ids {
  type        = list(string)
  default     = []
  description = "Ids de los security groups que se aplicaran a las instancias"
}

variable root_block_device {
  type        = list(map(string))
  default     = []
  description = "Configuracion de espacio en disco raiz"
}

variable user_data {
  type        = string
  default     = null
  description = "User data usado en el lanzamiento de la instancia."
}

variable user_data_base64 {
  type        = string
  default     = null
  description = "Pueder ser usado en lugar de user_data para pasar la data en formtato base64."
}

variable iam_instance_profile {
  type        = string
  default     = ""
  description = "Nombre del instance profile creado en iam de aws para asiagnar a la instancia"
}


variable tags {
  type        = map
  default     = {}
  description = "Tags que se asociaran a todas las instancias"
}

