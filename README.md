# Modulo de terraform para crear instancias de ec2

### Inputs

| Nombre  | Descripción  | Requerido | Default  |
|---|---|---|---|
| azs  | Lista de zonas de disponibilidad donde que se quieren crear recursos  | SI  | N/A  |
| ami  | Imagen que se usara en las instancias  | SI  | N/A  |
| subnets_ids  | Lista de ids de las subnets donde de crearan las instancias  | SI  | N/A  |
| key_name  | Nombre de key publica que se asociara a las instancias"  | SI  | N/A  |
| prefix_instance  | Nombre que tendra cada instancia. A este nombre se le agregara el numero de la instancia creada  | NO  | instance-  |
| instance_type  | Tipo de instancia que se crearan  | NO  | t2.micro  |
| instance_quantity  | Cantidad de instancias a crear  | NO  | 1  |
| vpc_security_group_ids  | Lista de ids de los security groups que se aplicaran a las instancias  | NO  | []  |
| root_block_device  | Configuracion de espacio en disco raiz  | NO  | []  |
| user_data  | User data usado en el lanzamiento de la instancia  | NO  | null  |
| user_data_base64  | Pueder ser usado en lugar de user_data para pasar la data en formtato base64  | NO  | null  |
| iam_instance_profile  | Nombre del creado en iam de aws para asiagnar a la instancia  | NO  | ""  |
| tags  | Tags que se asociaran a todas las instancias  | NO  | {}  |


### Outputs

| Nombre  | Descripción  |
|---|---|
| instances  | Objeto con informacion de las instancias, los atributos son los de la documentacion oficial del recurso aws_instance  |