resource "aws_instance" "ec2_instances" {

  count                = var.instance_quantity
  instance_type        = var.instance_type
  ami                  = var.ami
  user_data            = var.user_data
  user_data_base64     = var.user_data_base64
  subnet_id            = element(compact(var.subnets_ids), count.index)
  availability_zone    = element(compact(var.azs), count.index)
  iam_instance_profile = var.iam_instance_profile
  tags = merge(
    {
      Name = "${var.prefix_instance}-${count.index + 1}"
    },
    var.tags
  )
  vpc_security_group_ids = var.vpc_security_group_ids
  key_name               = var.key_name

  dynamic "root_block_device" {
    for_each = var.root_block_device
    content {
      delete_on_termination = lookup(root_block_device.value, "delete_on_termination", null)
      encrypted             = lookup(root_block_device.value, "encrypted", null)
      iops                  = lookup(root_block_device.value, "iops", null)
      kms_key_id            = lookup(root_block_device.value, "kms_key_id", null)
      volume_size           = lookup(root_block_device.value, "volume_size", null)
      volume_type           = lookup(root_block_device.value, "volume_type", null)
    }
  }

}